﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        var tmp = 0;
        var tmp2 = 0;
        int j = 0;
        for (int i = 0; i < x.Length; i++)
        {
            if (tmp < x[i])
            {
                tmp = x[i];
                j = i;
            }
        }
        for (int i = 0; i < x.Length; i++)
        {
            if (i == j)
            {
                i++;
                i--;
            }
            else
            {
                if (tmp2 < x[i])
                {
                    tmp2 = x[i];
                }
            }
        }
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        return tmp2;
    }
}