﻿using System.Text;

var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        var builder = new StringBuilder();
        for (int i = 0; i < Math.Max(value1.Length, value2.Length); i++)
        {
            if (i < value1.Length) { builder.Append(value1[i]); }
            if (i < value2.Length) { builder.Append(value2[i]); }
        }
        // xem file README.md
        return builder.ToString();
    }
}

